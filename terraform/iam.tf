resource "aws_iam_group" "ponisearch" {
  name = "ponisearch"
}

resource "aws_iam_user" "ponisearch" {
  name = "ponisearch"
}

resource "aws_iam_group_membership" "ponisearch" {
  name  = "ponisearch"
  group = aws_iam_group.ponisearch.name
  users = [aws_iam_user.ponisearch.name]
}

data "aws_iam_policy_document" "ponisearch_publish" {
  statement {
    effect    = "Allow"
    resources = [aws_sns_topic.ponisearch.arn]
    actions   = ["sns:Publish"]
  }
}

resource "aws_iam_group_policy" "ponisearch_sns" {
  name   = "ponisearch_sns"
  group  = aws_iam_group.ponisearch.name
  policy = data.aws_iam_policy_document.ponisearch_publish.json
}
