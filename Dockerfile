# build
FROM golang:alpine as build

WORKDIR /go/src/app
COPY . .

RUN CGO_ENABLED=no GOARCH=arm go build -o ponisearch

FROM alpine:latest

COPY --from=build /go/src/app/ponisearch /usr/local/bin/ponisearch
RUN apk add ca-certificates
ENTRYPOINT ["/usr/local/bin/ponisearch"]
