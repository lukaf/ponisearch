package alldogsmatter

import (
	"bytes"

	"github.com/anaskhan96/soup"
	"gitlab.com/lukaf/ponisearch/parsers"
)

const url = "https://alldogsmatter.co.uk/rehome/meet-our-dogs/"

var tags = map[string][]string{
	"dog":      {"div", "class", "grid-block"},
	"reserved": {"div", "class", "reserved-banner"},
}

func Dogs() ([]parsers.Dog, error) {
	data, err := soup.Get(url)
	if err != nil {
		return nil, err
	}

	output := make([]parsers.Dog, 0)

	doc := soup.HTMLParse(data)

	for _, entry := range doc.FindAll(tags["dog"]...) {
		if entry.Find(tags["reserved"]...).Pointer != nil {
			continue
		}

		link := entry.Find("a").Attrs()["href"]
		description := func() string {
			var buf bytes.Buffer

			for _, line := range entry.Find("p").Children() {
				if line.NodeValue == "br" {
					buf.WriteString("\n")
				} else {
					buf.WriteString(line.NodeValue)
				}
			}

			return buf.String()
		}()

		output = append(
			output,
			parsers.Dog{
				Link:        link,
				Description: description,
			},
		)
	}

	return output, nil
}
