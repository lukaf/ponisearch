package battersea

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/lukaf/ponisearch/parsers"
)

const url = "https://www.battersea.org.uk/api/animals/dogs"

type Result struct {
	Animals Animals `json:"animals"`
}

type Animals map[string]Animal

type Animal struct {
	Name     string `json:"title"`
	DOB      string `json:"field_animal_age"`
	Centre   string `json:"field_animal_centre"`
	Size     string `json:"field_animal_size"`
	Breed    string `json:"field_animal_breed"`
	Sex      string `json:"field_animal_sex"`
	Path     string `json:"path"`
	Reserved string `json:"field_animal_reserved"`
	Rehomed  string `json:"field_animal_rehomed"`
	Child    string `json:"field_animal_child_suitability"`
	Cat      string `json:"field_animal_cat_suitability"`
	Dog      string `json:"field_animal_dog_suitability"`
}

func (a *Animal) age() int {
	now := time.Now().UTC().Year()
	dob, err := time.Parse("2006-01-02", a.DOB)
	if err != nil {
		log.Printf("Error converting time string '%s': %s", a.DOB, err)
		return 0
	}

	return now - dob.Year()
}

func (a *Animal) Link() string {
	return "https://www.battersea.org.uk" + a.Path
}

func (a *Animal) available() bool {
	return a.Rehomed == "" && a.Reserved == ""
}

func (a *Animal) Description() string {
	return fmt.Sprintf(
		"%s\n%s\n%d year(s)\n%s\nChildren: %s\nCats: %s\nDogs: %s",
		a.Sex, a.Breed, a.age(), a.Size, a.Child, a.Cat, a.Dog)
}

func (a *Animal) String() string {
	return fmt.Sprintf("%s\n%s", a.Link(), a.Description())
}

func Dogs() ([]parsers.Dog, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	resp.Body.Close()

	var r Result
	if err := json.Unmarshal(data, &r); err != nil {
		return nil, err
	}

	var output []parsers.Dog
	for _, dog := range r.Animals {
		if dog.available() {
			output = append(
				output,
				parsers.Dog{
					Link:        dog.Link(),
					Description: dog.Description(),
				},
			)
		}
	}

	return output, nil
}
