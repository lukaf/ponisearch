package battersea

import (
	"testing"
)

func TestUrl(t *testing.T) {
	a := &Animal{
		Path: "/some/path",
	}
	want := "https://www.battersea.org.uk/some/path"

	if got := a.Link(); got != want {
		t.Errorf("URL building failed. Want '%s', got '%s'",
			got, want)
	}
}
