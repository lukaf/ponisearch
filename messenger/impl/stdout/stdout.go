package stdout

import "fmt"

type Messenger struct{}

func (m *Messenger) Send(msg string) error {
	fmt.Println(msg)
	return nil
}
