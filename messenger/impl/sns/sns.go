package sns

import (
	"errors"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	awssns "github.com/aws/aws-sdk-go/service/sns"
)

const (
	topic  = "arn:..."
	region = "eu-west-1"
)

type Messenger struct {
	TopicARN string
	Region   string
}

func (m *Messenger) init() error {
	if m.TopicARN == "" {
		return errors.New("SNS messenger error: emtpy topic ARN")
	}

	if m.Region == "" {
		return errors.New("SNS messenger error: emtpy region")
	}

	return nil
}

func (m *Messenger) Send(msg string) error {
	if err := m.init(); err != nil {
		return err
	}

	s, err := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region: aws.String(m.Region),
		},
	})
	if err != nil {
		return err
	}

	client := awssns.New(s)

	input := &awssns.PublishInput{
		Message:  aws.String(msg),
		Subject:  aws.String("Poni Search"),
		TopicArn: aws.String(m.TopicARN),
	}

	_, err = client.Publish(input)
	return err
}
