package messenger

import (
	"errors"
	"os"

	"gitlab.com/lukaf/ponisearch/messenger/impl/sns"
	"gitlab.com/lukaf/ponisearch/messenger/impl/stdout"
)

type Messenger interface {
	Send(msg string) error
}

var implementations = map[string]Messenger{
	"sns": &sns.Messenger{
		TopicARN: os.Getenv("MESSENGER_SNS_TOPIC_ARN"),
		Region:   os.Getenv("MESSENGER_SNS_REGION"),
	},
	"stdout": &stdout.Messenger{},
}

func Send(impl string, message string) error {
	if _, ok := implementations[impl]; !ok {
		return errors.New("unknown implementation")
	}

	return implementations[impl].Send(message)
}
