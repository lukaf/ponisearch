package messenger

import "testing"

type FakeMessenger struct {
	output string
}

func (m *FakeMessenger) Send(msg string) error {
	m.output = msg
	return nil
}

func TestSend(t *testing.T) {
	messenger := &FakeMessenger{}
	implementations["fake"] = messenger

	err := Send("fake", "hello")
	if err != nil {
		t.Fatal("unexpected error", err)
	}

	if messenger.output != "hello" {
		t.Fatalf("expected message 'hello', got '%s'\n", messenger.output)
	}

	err = Send("nonexistent", "hello")
	if err == nil {
		t.Fatal("selecting an unknown messenger implementation should throw an error")
	}
}
