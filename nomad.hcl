job "ponisearch" {
  datacenters = ["dc1"]

  type = "service"

  group "one" {
    task "notifier" {
      driver = "docker"

      config {
        image = "registry.gitlab.com/lukaf/ponisearch:latest"
        force_pull = true
      }

      env {
        MESSENGER_SNS_TOPIC_ARN = "arn:aws:sns:eu-west-2:397060331789:ponisearch"
        MESSENGER_SNS_REGION = "eu-west-2"
        AWS_ACCESS_KEY = "${PONISEARCH_AWS_ACCESS_KEY}"
        AWS_SECRET_ACCESS_KEY = "${PONISEARCH_AWS_SECRET_KEY_ID}"
      }
    }
  }
}
