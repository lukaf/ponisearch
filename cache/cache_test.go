package cache

import (
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	tests := []struct {
		description string
		input       []string
		output      *Cache
	}{
		{
			"Create empty cache",
			nil,
			&Cache{},
		},
		{
			"Create cache with items",
			[]string{"hello", "world"},
			&Cache{
				Items: map[string]Times{
					"hello": {},
					"world": {},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			out := New(test.input...)
			if out.Mutex == nil {
				t.Fatal("cache is missing mutex")
			}

			if len(out.Items) != len(test.output.Items) {
				t.Fatalf("Expecting empty cache, got %d items\n", len(test.output.Items))
			}
		})
	}

}

func TestAdd(t *testing.T) {
	tests := []struct {
		description string
		items       []string
		count       int // always increases as we never clear the cache
	}{
		{
			"Add one item",
			[]string{"one"},
			1,
		},
		{
			"Add the second item",
			[]string{"two"},
			2,
		},
		{
			"Add two more items",
			[]string{"three", "four"},
			4,
		},
	}

	c := New()

	for _, test := range tests {
		t.Run(test.description, func(t *testing.T) {
			c.Add(test.items...)
			if len(c.Items) != test.count {
				t.Fatalf("expected %d item(s), got %d\n", test.count, len(c.Items))
			}
		})
	}
}

func TestDelete(t *testing.T) {
	c := New("hello", "world", "!")
	if len(c.Items) != 3 {
		t.Fatalf("expected 3 items, got %d\n", len(c.Items))
	}

	c.Delete("hello")
	if len(c.Items) != 2 {
		t.Fatalf("expected 2 items, got %d\n", len(c.Items))
	}

	c.Delete("world", "!")
	if len(c.Items) != 0 {
		t.Fatalf("expected 0 items, got %d\n", len(c.Items))
	}
}

func TestExists(t *testing.T) {
	c := New("one", "two")

	if c.Exists("three") {
		t.Fatal("element 'three' shouldn't exist")
	}

	if !c.Exists("one") {
		t.Fatal("element 'one' should exist")
	}
}

func TestPurge(t *testing.T) {
	now := time.Now().UTC()
	c := New("one", "two")

	c.Purge()
	if len(c.Items) != 2 {
		t.Fatal("no elements should be removed from the cache")
	}

	c.Items["one"] = Times{
		ExpiresAt: now.Add(time.Minute * -1),
	}
	c.Purge()
	if len(c.Items) != 1 {
		t.Fatal("an element should be removed from the cache")
	}

	if !c.Exists("two") {
		t.Fatal("element 'two' should exist")
	}

	if c.Exists("one") {
		t.Fatal("element 'one' should not exist")
	}
}
