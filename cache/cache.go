package cache

import (
	"sync"
	"time"
)

type Items map[string]Times

type Times struct {
	CreatedAt time.Time
	ExpiresAt time.Time
}

type Cache struct {
	Items         Items
	Mutex         *sync.Mutex
	CreatedAt     time.Time
	CacheDuration time.Duration
}

func New(elements ...string) *Cache {
	now := time.Now().UTC()
	items := make(Items)
	cacheDuration := time.Hour * 24

	c := &Cache{
		Items:         items,
		Mutex:         &sync.Mutex{},
		CreatedAt:     now,
		CacheDuration: cacheDuration,
	}

	for _, element := range elements {
		c.Items[element] = Times{
			CreatedAt: now,
			ExpiresAt: now.Add(cacheDuration),
		}
	}

	return c
}

func (c *Cache) Add(elements ...string) {
	now := time.Now().UTC()
	c.Mutex.Lock()
	for _, element := range elements {
		c.Items[element] = Times{
			CreatedAt: now,
			ExpiresAt: now.Add(c.CacheDuration),
		}
	}
	c.Mutex.Unlock()
}

func (c *Cache) Delete(elements ...string) {
	for _, element := range elements {
		if _, ok := c.Items[element]; ok {
			c.Mutex.Lock()
			delete(c.Items, element)
			c.Mutex.Unlock()
		}
	}
}

func (c *Cache) Exists(element string) bool {
	_, ok := c.Items[element]
	return ok
}

func (c *Cache) Purge() {
	purgable := make([]string, 0)
	now := time.Now().UTC()
	for item, times := range c.Items {
		if now.After(times.ExpiresAt) {
			purgable = append(purgable, item)
		}
	}

	c.Delete(purgable...)
}
