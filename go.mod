module gitlab.com/lukaf/ponisearch

go 1.15

require (
	github.com/anaskhan96/soup v1.2.4
	github.com/aws/aws-sdk-go v1.35.1
)
