package main

import (
	"bytes"
	"fmt"
	"log"
	"time"

	"gitlab.com/lukaf/ponisearch/cache"
	"gitlab.com/lukaf/ponisearch/messenger"
	"gitlab.com/lukaf/ponisearch/parsers"
	"gitlab.com/lukaf/ponisearch/parsers/alldogsmatter"
	"gitlab.com/lukaf/ponisearch/parsers/battersea"
)

func main() {
	tick := time.NewTicker(time.Hour * 1)

	c := cache.New()

	for {
		select {
		case <-tick.C:
			var dogs []parsers.Dog

			if i, err := alldogsmatter.Dogs(); err == nil {
				dogs = append(dogs, i...)
			} else {
				log.Println("Unable to get data from All Dogs Matter:", err)
			}

			if i, err := battersea.Dogs(); err == nil {
				dogs = append(dogs, i...)
			} else {
				log.Println("Unable to get data from Battersea:", err)
			}

			if len(dogs) == 0 {
				continue
			}

			var message bytes.Buffer

			for _, dog := range dogs {
				if c.Exists(dog.Link) {
					continue
				}

				c.Add(dog.Link)
				message.WriteString(fmt.Sprintf("%s\n%s\n\n",
					dog.Link, dog.Description))
			}

			if message.Len() > 0 {
				if err := messenger.Send("sns", message.String()); err != nil {
					log.Println("Unable to send message:", err)
				}
			}
		}
	}
}
